package com.dstz.bus.dao;

import com.dstz.base.dao.BaseDao;
import com.dstz.bus.model.BusColumnCtrl;
import org.mybatis.spring.annotation.MapperScan;

@MapperScan
public abstract interface BusColumnCtrlDao extends BaseDao<String, BusColumnCtrl>
{
  public abstract void removeByTableId(String paramString);

  public abstract BusColumnCtrl getByColumnId(String paramString);
}