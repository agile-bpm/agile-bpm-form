package com.dstz.bus.manager;

import com.dstz.base.manager.Manager;
import com.dstz.bus.model.BusColumnCtrl;

public abstract interface BusColumnCtrlManager extends Manager<String, BusColumnCtrl>
{
  public abstract void removeByTableId(String paramString);

  public abstract BusColumnCtrl getByColumnId(String paramString);
}