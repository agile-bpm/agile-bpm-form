package com.dstz.bus.executor;

import com.dstz.base.api.executor.Executor.TYPE;
import com.dstz.base.core.util.AppUtil;
import org.springframework.stereotype.Service;

@Service
public class DbAfterSaveDataScriptExecutor extends DbAfterSaveDataExecuteChain
{
  public String getName()
  {
    return "BussinessDbService运行save方法后运行脚本的插件";
  }

  public String type()
  {
    return Executor.TYPE.UNNECESSARY.getKey();
  }

  protected void a(SaveDataParam param)
  {
    ((DbBeforeSaveDataScriptExecutor)AppUtil.getBean(DbBeforeSaveDataScriptExecutor.class)).a(param);
  }

  public String getCheckerKey()
  {
    return "FreePluginChecker";
  }

  public int getSn()
  {
    return 0;
  }
}