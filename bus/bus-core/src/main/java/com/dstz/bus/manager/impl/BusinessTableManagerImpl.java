package com.dstz.bus.manager.impl;

import com.dstz.base.api.query.QueryFilter;
import com.dstz.base.api.query.QueryOP;
import com.dstz.base.core.util.string.StringUtil;
import com.dstz.base.db.datasource.DbContextHolder;
import com.dstz.base.db.id.UniqueIdUtil;
import com.dstz.base.db.model.query.DefaultQueryFilter;
import com.dstz.base.db.tableoper.TableOperator;
import com.dstz.base.db.tableoper.TableOperatorFactory;
import com.dstz.base.manager.impl.BaseManager;
import com.dstz.bus.dao.BusinessTableDao;
import com.dstz.bus.manager.BusColumnCtrlManager;
import com.dstz.bus.manager.BusinessColumnManager;
import com.dstz.bus.manager.BusinessTableManager;
import com.dstz.bus.model.BusColumnCtrl;
import com.dstz.bus.model.BusinessColumn;
import com.dstz.bus.model.BusinessTable;
import com.dstz.bus.util.BusinessTableCacheUtil;
import com.dstz.sys.api2.service.ISysDataSourceService;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class BusinessTableManagerImpl extends BaseManager<String, BusinessTable>
  implements BusinessTableManager
{

  @Autowired
  BusinessTableDao l;

  @Autowired
  BusinessColumnManager m;

  @Autowired
  BusColumnCtrlManager n;

  @Autowired
  ISysDataSourceService o;

  public void save(BusinessTable businessTable)
  {
    if (StringUtil.isEmpty(businessTable.getId())) {
      businessTable.setId(UniqueIdUtil.getSuid());
      create(businessTable);
    } else {
      update(businessTable);
      this.n.removeByTableId(businessTable.getId());
      this.m.removeByTableId(businessTable.getId());
    }
    Iterator localIterator = businessTable.getColumns().iterator();
    while (true) { BusinessColumn businessColumn;
      BusColumnCtrl ctrl;
      while (true) { if (!(localIterator.hasNext())) break label185; businessColumn = (BusinessColumn)localIterator.next();
        if (StringUtil.isEmpty(businessColumn.getId()))
          businessColumn.setId(UniqueIdUtil.getSuid());

        businessColumn.setTable(businessTable);
        businessColumn.setTableId(businessTable.getId());
        this.m.create(businessColumn);
        ctrl = businessColumn.getCtrl();
        if (!(businessColumn.isPrimary()))
          break;
      }
      if (StringUtil.isEmpty(ctrl.getId()))
        ctrl.setId(UniqueIdUtil.getSuid());

      ctrl.setColumnId(businessColumn.getId());
      this.n.create(businessColumn.getCtrl());
    }

    label185: newTableOperator(businessTable).syncColumn();

    BusinessTableCacheUtil.put(businessTable);
  }

  public BusinessTable getByKey(String key)
  {
    QueryFilter filter = new DefaultQueryFilter();
    filter.addFilter("key_", key, QueryOP.EQUAL);
    return ((BusinessTable)queryOne(filter));
  }

  private void a(BusinessTable businessTable)
  {
    if (businessTable == null) {
      return;
    }

    List columns = this.m.getByTableId(businessTable.getId());
    for (Iterator localIterator = columns.iterator(); localIterator.hasNext(); ) { BusinessColumn column = (BusinessColumn)localIterator.next();
      column.setCtrl(this.n.getByColumnId(column.getId()));
      column.setTable(businessTable);
    }
    businessTable.setColumns(columns);

    TableOperator tableOperator = newTableOperator(businessTable);
    businessTable.setCreatedTable(tableOperator.isTableCreated());
  }

  public TableOperator newTableOperator(BusinessTable businessTable)
  {
    JdbcTemplate jdbcTemplate = this.o.getJdbcTemplateByKey(businessTable.getDsKey());
    return TableOperatorFactory.newOperator(DbContextHolder.getDataSourceDbType(businessTable.getDsKey()), businessTable, jdbcTemplate);
  }

  public BusinessTable getFilledByKey(String key)
  {
    BusinessTable businessTable = BusinessTableCacheUtil.get(key);
    if (businessTable != null)
      return businessTable;

    businessTable = getByKey(key);
    a(businessTable);
    BusinessTableCacheUtil.put(businessTable);
    return businessTable;
  }
}