package com.dstz.bus.executor;

import com.alibaba.fastjson.JSONObject;
import com.dstz.bus.model.BusinessData;
import com.dstz.bus.model.BusinessObject;

public class SaveDataParam
{
  private BusinessData a;
  private JSONObject b;
  private BusinessObject c;
  private JSONObject d;

  public SaveDataParam(BusinessData businessData, JSONObject data, BusinessObject businessObject, JSONObject permission)
  {
    this.a = businessData;
    this.b = data;
    this.c = businessObject;
    this.d = permission;
  }

  public JSONObject getData() {
    return this.b;
  }

  public BusinessObject getBusinessObject() {
    return this.c;
  }

  public JSONObject getPermission() {
    return this.d;
  }

  public BusinessData getBusinessData() {
    return this.a;
  }
}