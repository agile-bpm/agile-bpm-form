package com.dstz.bus.executor;

import com.dstz.base.api.executor.Executor.TYPE;
import org.springframework.stereotype.Service;

@Service
public class DbBeforeSaveDataScriptExecutor extends DbBeforeSaveDataExecuteChain
{
  public String getName()
  {
    return "BussinessDbService运行save方法前运行脚本的插件";
  }

  public String type()
  {
    return Executor.TYPE.UNNECESSARY.getKey();
  }

  protected void a(SaveDataParam param)
  {
  }

  public String getCheckerKey()
  {
    return "FreePluginChecker";
  }

  public int getSn()
  {
    return 0;
  }
}