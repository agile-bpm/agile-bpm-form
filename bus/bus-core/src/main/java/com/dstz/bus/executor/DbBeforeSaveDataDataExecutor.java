package com.dstz.bus.executor;

import com.alibaba.fastjson.JSONObject;
import com.dstz.base.api.executor.Executor.TYPE;
import com.dstz.bus.model.BusinessData;
import org.springframework.stereotype.Service;

@Service
public class DbBeforeSaveDataDataExecutor extends DbBeforeSaveDataExecuteChain
{
  public String getName()
  {
    return "BussinessDbService运行save方法前处理data(json格式那个)的插件";
  }

  public String type()
  {
    return Executor.TYPE.NECESSARY.getKey();
  }

  protected void a(SaveDataParam param)
  {
    BusinessData businessData = param.getBusinessData();
    JSONObject data = param.getData();
  }

  public String getCheckerKey()
  {
    return "FreePluginChecker";
  }

  public int getSn()
  {
    return 2;
  }
}