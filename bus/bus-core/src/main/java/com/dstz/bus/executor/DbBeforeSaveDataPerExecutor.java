package com.dstz.bus.executor;

import com.alibaba.fastjson.JSONObject;
import com.dstz.base.api.executor.Executor.TYPE;
import com.dstz.bus.model.BusinessData;
import org.springframework.stereotype.Service;

@Service
public class DbBeforeSaveDataPerExecutor extends DbBeforeSaveDataExecuteChain
{
  public String getName()
  {
    return "BussinessDbService运行save方法前处理permission的插件";
  }

  public String type()
  {
    return Executor.TYPE.UNNECESSARY.getKey();
  }

  protected void a(SaveDataParam param)
  {
    BusinessData businessData = param.getBusinessData();
    JSONObject permission = param.getPermission();
  }

  public String getCheckerKey()
  {
    return "FreePluginChecker";
  }

  public int getSn()
  {
    return 3;
  }
}