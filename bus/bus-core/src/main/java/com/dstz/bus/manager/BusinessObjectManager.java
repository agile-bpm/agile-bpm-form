package com.dstz.bus.manager;

import com.alibaba.fastjson.JSONObject;
import com.dstz.base.manager.Manager;
import com.dstz.bus.model.BusinessObject;
import java.util.List;

public abstract interface BusinessObjectManager extends Manager<String, BusinessObject>
{
  public abstract BusinessObject getByKey(String paramString);

  public abstract List<JSONObject> boTreeData(String paramString);

  public abstract BusinessObject getFilledByKey(String paramString);
}