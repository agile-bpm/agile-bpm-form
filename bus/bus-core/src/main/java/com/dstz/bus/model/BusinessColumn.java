package com.dstz.bus.model;

import com.dstz.base.api.constant.ColumnType;
import com.dstz.base.api.exception.BusinessException;
import com.dstz.base.api.model.IBaseModel;
import com.dstz.base.core.util.string.StringUtil;
import com.dstz.base.core.util.time.DateFormatUtil;
import com.dstz.base.db.model.table.Column;
import com.dstz.bus.api.model.IBusinessColumn;
import java.util.Date;
import javax.validation.Valid;
import org.hibernate.validator.constraints.NotEmpty;

public class BusinessColumn extends Column
  implements IBaseModel, IBusinessColumn
{

  @NotEmpty
  private String id;

  @NotEmpty
  private String key;

  @NotEmpty
  private String y;

  @Valid
  private BusColumnCtrl z;
  private BusinessTable v;

  public String getId()
  {
    return this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getKey() {
    return this.key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public BusColumnCtrl getCtrl() {
    return this.z;
  }

  public void setCtrl(BusColumnCtrl ctrl) {
    this.z = ctrl;
  }

  public String getTableId() {
    return this.y;
  }

  public void setTableId(String tableId) {
    this.y = tableId;
  }

  public BusinessTable getTable()
  {
    return this.v;
  }

  public void setTable(BusinessTable table) {
    this.v = table;
  }

  public Object initValue()
  {
    if (StringUtil.isEmpty(this.defaultValue))
      return null;

    Object initValue = null;
    if ((ColumnType.VARCHAR.equalsWithKey(this.type)) || (ColumnType.CLOB.equalsWithKey(this.type)))
      initValue = this.defaultValue;

    if (ColumnType.NUMBER.equalsWithKey(this.type))
      initValue = Double.valueOf(Double.parseDouble(this.defaultValue));

    if (ColumnType.DATE.equalsWithKey(this.type))
      try {
        initValue = DateFormatUtil.parse(this.defaultValue);
      } catch (Exception e) {
        throw new BusinessException(e);
      }


    return initValue;
  }

  public Date getCreateTime()
  {
    return null;
  }

  public void setCreateTime(Date createTime)
  {
  }

  public String getCreateBy()
  {
    return null;
  }

  public void setCreateBy(String createBy)
  {
  }

  public Date getUpdateTime()
  {
    return null;
  }

  public void setUpdateTime(Date updateTime)
  {
  }

  public String getUpdateBy()
  {
    return null;
  }

  public void setUpdateBy(String updateBy)
  {
  }
}