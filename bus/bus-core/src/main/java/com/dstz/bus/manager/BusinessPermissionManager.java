package com.dstz.bus.manager;

import com.dstz.base.manager.Manager;
import com.dstz.bus.model.BusinessPermission;

public abstract interface BusinessPermissionManager extends Manager<String, BusinessPermission>
{
  public abstract BusinessPermission getByObjTypeAndObjVal(String paramString1, String paramString2);

  public abstract BusinessPermission getByObjTypeAndObjVal(String paramString1, String paramString2, String paramString3);
}