package com.dstz.bus.manager.impl;

import com.alibaba.fastjson.JSONObject;
import com.dstz.base.api.query.QueryFilter;
import com.dstz.base.api.query.QueryOP;
import com.dstz.base.core.util.BeanUtils;
import com.dstz.base.db.model.query.DefaultQueryFilter;
import com.dstz.base.manager.impl.BaseManager;
import com.dstz.bus.api.constant.BusTableRelType;
import com.dstz.bus.dao.BusinessObjectDao;
import com.dstz.bus.manager.BusinessObjectManager;
import com.dstz.bus.manager.BusinessTableManager;
import com.dstz.bus.model.BusTableRel;
import com.dstz.bus.model.BusinessColumn;
import com.dstz.bus.model.BusinessObject;
import com.dstz.bus.model.BusinessTable;
import com.dstz.bus.service.BusinessPermissionService;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BusinessObjectManagerImpl extends BaseManager<String, BusinessObject>
  implements BusinessObjectManager
{

  @Resource
  BusinessObjectDao g;

  @Autowired
  BusinessTableManager h;

  @Autowired
  BusinessPermissionService i;

  public BusinessObject getByKey(String key)
  {
    QueryFilter filter = new DefaultQueryFilter();
    filter.addFilter("key_", key, QueryOP.EQUAL);
    return ((BusinessObject)queryOne(filter));
  }

  public BusinessObject getFilledByKey(String key)
  {
    BusinessObject businessObject = getByKey(key);
    a(businessObject);
    return businessObject;
  }

  private void a(BusinessObject businessObject)
  {
    if (businessObject == null) {
      return;
    }

    for (Iterator localIterator = businessObject.getRelation().list().iterator(); localIterator.hasNext(); ) { BusTableRel rel = (BusTableRel)localIterator.next();
      rel.setTable(this.h.getFilledByKey(rel.getTableKey()));
      rel.setBusObj(businessObject);
    }

    a(businessObject.getRelation());
  }

  private void a(BusTableRel rel)
  {
    for (Iterator localIterator = rel.getChildren().iterator(); localIterator.hasNext(); ) { BusTableRel r = (BusTableRel)localIterator.next();
      r.setParent(rel);
      a(r);
    }
  }

  public List<JSONObject> boTreeData(String key)
  {
    BusinessObject businessObject = getByKey(key);
    BusTableRel busTableRel = businessObject.getRelation();
    List list = new ArrayList();
    a(busTableRel, "0", list);

    if (BeanUtils.isNotEmpty(list))
      ((JSONObject)list.get(0)).put("alias", key);

    return list;
  }

  private void a(BusTableRel busTableRel, String parentId, List<JSONObject> list)
  {
    BusinessTable businessTable = this.h.getFilledByKey(busTableRel.getTableKey());
    JSONObject root = new JSONObject();
    root.put("id", businessTable.getId());
    root.put("key", businessTable.getKey());
    root.put("name", businessTable.getName() + "(" + BusTableRelType.getByKey(busTableRel.getType()).getDesc() + ")");
    root.put("parentId", parentId);
    root.put("nodeType", "table");
    list.add(root);

    for (Iterator localIterator = businessTable.getColumns().iterator(); localIterator.hasNext(); ) { BusinessColumn businessColumn = (BusinessColumn)localIterator.next();
      JSONObject columnJson = new JSONObject();
      columnJson.put("id", businessColumn.getId());
      columnJson.put("key", businessColumn.getName());
      columnJson.put("name", businessColumn.getComment());
      columnJson.put("tableKey", businessTable.getKey());
      columnJson.put("parentId", businessTable.getId());
      columnJson.put("nodeType", "column");
      list.add(columnJson);
    }
    for (localIterator = busTableRel.getChildren().iterator(); localIterator.hasNext(); ) { BusTableRel rel = (BusTableRel)localIterator.next();
      a(rel, businessTable.getId(), list);
    }
  }
}