package com.dstz.bus.manager;

import com.dstz.base.manager.Manager;
import com.dstz.bus.model.BusinessColumn;
import java.util.List;

public abstract interface BusinessColumnManager extends Manager<String, BusinessColumn>
{
  public abstract void removeByTableId(String paramString);

  public abstract List<BusinessColumn> getByTableId(String paramString);
}