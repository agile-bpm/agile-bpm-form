package com.dstz.bus.executor;

import com.dstz.base.core.executor.AbstractExecuteChain;

public abstract class DbAfterSaveDataExecuteChain extends AbstractExecuteChain<SaveDataParam>
{
}