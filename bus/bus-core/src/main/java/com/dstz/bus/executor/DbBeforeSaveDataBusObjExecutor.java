package com.dstz.bus.executor;

import com.dstz.base.api.executor.Executor.TYPE;
import com.dstz.bus.model.BusinessData;
import com.dstz.bus.model.BusinessObject;
import org.springframework.stereotype.Service;

@Service
public class DbBeforeSaveDataBusObjExecutor extends DbBeforeSaveDataExecuteChain
{
  public String getName()
  {
    return "BussinessDbService运行save方法前处理的插件";
  }

  public String type()
  {
    return Executor.TYPE.NECESSARY.getKey();
  }

  protected void a(SaveDataParam param)
  {
    BusinessData businessData = param.getBusinessData();
    BusinessObject businessObject = param.getBusinessObject();
  }

  public String getCheckerKey()
  {
    return "FreePluginChecker";
  }

  public int getSn()
  {
    return 1;
  }
}