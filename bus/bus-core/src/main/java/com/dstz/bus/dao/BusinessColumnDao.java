package com.dstz.bus.dao;

import com.dstz.base.dao.BaseDao;
import com.dstz.bus.model.BusinessColumn;
import java.util.List;
import org.mybatis.spring.annotation.MapperScan;

@MapperScan
public abstract interface BusinessColumnDao extends BaseDao<String, BusinessColumn>
{
  public abstract void removeByTableId(String paramString);

  public abstract List<BusinessColumn> getByTableId(String paramString);
}